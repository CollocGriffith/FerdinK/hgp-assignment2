from PyQt5.QtCore import QSize, Qt, QRect
from PyQt5.QtGui import QIcon, QPixmap, QBrush, QPen, QImage, QImageReader, QMouseEvent, QPainter
from PyQt5.QtWidgets import QMainWindow, QAction, QGridLayout, QLabel, QGroupBox, QScrollArea, QWidget, \
    QPushButton, QListWidget, QListWidgetItem, QMenu, QAbstractItemView, QComboBox, QSlider, \
    QDoubleSpinBox, QColorDialog, QFormLayout, QDialog, QDialogButtonBox, QLineEdit, QFileDialog
from functools import partial


class AboutDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        layout = QFormLayout(self)
        layout.addRow(QLabel("Version: 1.0.0"))
        layout.addRow(QLabel("Created by Kevin FERDINAND - 2981444"))
        button_box = QDialogButtonBox(QDialogButtonBox.Close, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.rejected.connect(self.close)


class InfoDialog(QDialog):
    def __init__(self, parent=None, title="", message=""):
        super().__init__(parent)

        layout = QFormLayout(self)

        self.setWindowTitle(title)
        layout.addRow(QLabel(message))

        button_box = QDialogButtonBox(QDialogButtonBox.Ok, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.accepted.connect(self.btn_click)

    def btn_click(self):
        self.close()


class YesNoDialog(QDialog):
    def __init__(self, parent=None, title="", message=""):
        super().__init__(parent)

        layout = QFormLayout(self)

        self.setWindowTitle(title)
        layout.addRow(QLabel(message))

        button_box = QDialogButtonBox(QDialogButtonBox.Yes | QDialogButtonBox.No, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.accepted.connect(self.accepted)
        button_box.rejected.connect(self.rejected)
        self.isAccepted = False

    def is_accepted(self):
        return self.isAccepted

    def accepted(self):
        self.isAccepted = True
        self.close()

    def rejected(self):
        self.close()


class SizeDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        layout = QFormLayout(self)

        label_width = QLabel("Width :")
        self.le_width = QLineEdit(self)
        layout.addRow(label_width, self.le_width)

        label_height = QLabel("Height :")
        self.le_height = QLineEdit(self)
        layout.addRow(label_height, self.le_height)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.accepted.connect(self.accepted)
        button_box.rejected.connect(self.rejected)

        self.isAccepted = False

    def accepted(self):
        if not self.le_width.text().isdigit() or not self.le_height.text().isdigit():
            self.info_dialog("Error", "Width and Height must be filled and must be digits only.")
        else:
            self.isAccepted = True
            self.close()

    def rejected(self):
        self.close()

    @staticmethod
    def info_dialog(title, message):
        info_dialog = InfoDialog(title=title, message=message)
        info_dialog.exec()

    def is_accepted(self):
        return self.isAccepted

    def get_width(self):
        return int(self.le_width.text())

    def get_height(self):
        return int(self.le_height.text())


class Layer():
    def __init__(self, image, name):
        self.image = image
        self.visible = True
        self.name = name


class Image:
    def __init__(self, path=True, file_path=None, width=0, height=0):
        self.layers = []
        if path:
            reader = QImageReader(file_path)
            reader.setAutoTransform(True)
            if reader.canRead():
                self.valid = True
                image = reader.read()
                image.convertToFormat(QImage.Format_ARGB32)
                self.width = image.width()
                self.height = image.height()
                self.img_format = QImage.Format_ARGB32
                self.layers.append(Layer(image, "Base layer"))
            else:
                self.valid = False
        else:
            self.layers.append(Layer(QImage(width, height, QImage.Format_ARGB32), "Base layer"))
            self.layers[0].image.fill(Qt.white)
            self.width = width
            self.height = height
            self.img_format = QImage.Format_ARGB32
            self.valid = True

    def is_valid(self):
        return self.valid

    def delete_layer(self, position):
        del self.layers[position]

    def add_layer(self, position):
        layer = QImage(self.width, self.height, self.img_format)
        layer.fill(Qt.transparent)
        self.layers.insert(position, Layer(layer, "Layer " + str(self.get_layer_nbr())))

    def delete_all_layers(self):
        while len(self.layers) > 1:
            self.layers.pop(len(self.layers) - 1)
        self.layers[0].image.fill(Qt.white)

    def get_image(self, drawing_layer=None, drawing_layer_position=-1):
        image = QImage(self.width, self.height, self.img_format)
        image.fill(Qt.transparent)
        painter = QPainter(image)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        index = 0
        for layer in self.layers:
            if layer.visible:
                painter.drawImage(0, 0, layer.image)
                if drawing_layer and drawing_layer_position == index:
                    painter.drawImage(0, 0, drawing_layer)
            index += 1
        painter.end()
        return image

    def draw(self, image, position):
        painter = QPainter(self.layers[position].image)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage(0, 0, image)
        painter.end()

    def get_layer_nbr(self):
        return len(self.layers)

    def get_layers(self):
        return self.layers


class ImageViewer(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.drawing = False
        self.last_pos = None
        self.tool = "brush"
        self.pen_size = 1
        self.line_style = Qt.SolidLine
        self.cap_style = Qt.SquareCap
        self.join_style = Qt.BevelJoin
        self.pen_color = Qt.black
        self.viewer = QLabel()
        self.layout = QGridLayout()
        self.layout.addWidget(self.viewer)
        self.setLayout(self.layout)
        self.drawing_layer = None
        self.image = None
        self.pos = 0
        self.saved = True
        self.open_format = "Images (*.bmp *.gif *.jpg *.jpeg *.png)"
        self.save_format = "Windows bitmap (.bmp);;Graphic Interchange Format (.gif);;Joint Photographic Experts " + \
                           "Group(.jpg);;Joint Photographic Experts Group (.jpeg);;Portable Network Graphics (.png)"

    def draw_image(self):
        if self.drawing_layer is None:
            self.drawing_layer = QImage(self.image.width, self.image.height, QImage.Format_ARGB32)
            self.drawing_layer.fill(Qt.transparent)
        to_display = self.image.get_image(self.drawing_layer, self.pos)
        self.viewer.setPixmap(QPixmap.fromImage(to_display))
        self.viewer.resize(to_display.size())
        self.resize(to_display.size())

    def mousePressEvent(self, event: QMouseEvent):
        if event.button() == Qt.LeftButton and self.viewer.underMouse():
            self.drawing = True
            self.last_pos = self.viewer.mapFromGlobal(self.mapToGlobal(event.pos()))

    def mouseReleaseEvent(self, event: QMouseEvent):
        if event.button() == Qt.LeftButton:
            self.drawing = False
            self.last_pos = None
            self.image.draw(self.drawing_layer, self.pos)
            self.drawing_layer.fill(Qt.transparent)

    def mouseMoveEvent(self, event: QMouseEvent):
        if (event.buttons() & Qt.LeftButton) & self.drawing:
            pos = self.viewer.mapFromGlobal(self.mapToGlobal(event.pos()))
            if self.tool == "brush":
                self.brush_tool(pos)
            elif self.tool == "line":
                self.line_tool(pos)
            elif self.tool == "square":
                self.square_tool(pos)
            elif self.tool == "circle":
                self.circle_tool(pos)
            self.draw_image()

    def brush_tool(self, position):
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawLine(self.last_pos, position)
        self.last_pos = position
        painter.end()
        self.saved = False

    def line_tool(self, position):
        self.drawing_layer.fill(Qt.transparent)
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawLine(self.last_pos, position)
        painter.end()
        self.saved = False

    def square_tool(self, position):
        self.drawing_layer.fill(Qt.transparent)
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawRect(QRect(self.last_pos, position))
        painter.end()
        self.saved = False

    def circle_tool(self, position):
        self.drawing_layer.fill(Qt.transparent)
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawEllipse(QRect(self.last_pos, position))
        painter.end()
        self.saved = False

    @staticmethod
    def info_dialog(title, message):
        info_dialog = InfoDialog(title=title, message=message)
        info_dialog.exec()

    def new(self):
        size_dialog = SizeDialog()
        size_dialog.exec()
        if size_dialog.is_accepted():
            width = size_dialog.get_width()
            height = size_dialog.get_height()
            self.image = Image(path=False, width=width, height=height)
            if self.image.is_valid():
                self.draw_image()
                self.saved = False
            else:
                ImageViewer.info_dialog("Error", "Something went wrong when loading image, try again.")

    def open(self):
        if not self.saved:
            self.ask_save()
        file, _ = QFileDialog.getOpenFileName(self, "Select image", "/home", self.open_format)
        if file:
            self.image = Image(file_path=file)
            self.draw_image() if self.image.is_valid() else \
                ImageViewer.info_dialog("Error", "Something went wrong when loading image, try again.")

    def save(self):
        file, ext = QFileDialog.getSaveFileName(self, "Select where to save image", "/home", self.save_format)
        if file and ext:
            self.image.get_image().save(file + ext.split('(')[1].split(')')[0])
            self.saved = True

    def ask_save(self):
        ask_dialog = YesNoDialog(title="Work not saved", message="You have unsaved changes, save now ?")
        ask_dialog.exec()
        if ask_dialog.is_accepted():
            self.save()

    def clear(self):
        if self.image is not None:
            self.image.delete_all_layers()
            self.draw_image()

    def quit(self):
        if not self.saved:
            self.ask_save()

    def add_layer(self, position):
        self.image.add_layer(position)
        self.saved = False

    def delete_layer(self, position):
        self.image.delete_layer(position)
        self.draw_image()

    def get_layers(self):
        return self.image.get_layers()

    def get_layer_nbr(self):
        return self.image.get_layer_nbr()

    def set_thickness(self, thickness):
        self.pen_size = thickness

    def set_line_style(self, style):
        self.line_style = style

    def set_cap_style(self, style):
        self.cap_style = style

    def set_join_style(self, style):
        self.join_style = style

    def set_color(self, color):
        self.pen_color = color


class Paint(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.brush_join_type = QComboBox()
        self.brush_cap_type = QComboBox()
        self.grid = QGridLayout()
        self.layer_list = QListWidget()
        self.viewer_container = ImageViewer(self)
        self.brush_thickness_text = QDoubleSpinBox()
        self.add_layer_button = QPushButton()
        self.brush_line_type = QComboBox()
        self.remove_layer_button = QPushButton()
        self.brush_thickness = QSlider(Qt.Horizontal)
        self.setup_ui()
        self.visible_icon = QIcon("img/visible.png")
        self.hidden_icon = QIcon("img/hidden.png")
        self.saved = False

    def current_layer(self):
        return self.layer_list.currentRow()

    def update_layer_list(self):
        if self.viewer_container.image is not None:
            self.layer_list.clear()
            for layer in self.viewer_container.get_layers():
                list_item = QListWidgetItem()
                list_item.setIcon(self.visible_icon if layer.visible else self.hidden_icon)
                list_item.setText(layer.name)
                list_item.setFlags(list_item.flags() | Qt.ItemIsEditable)
                self.layer_list.addItem(list_item)
            self.layer_list.setCurrentRow(0)
            self.add_layer_button.setDisabled(False)

    @staticmethod
    def info_dialog(title, message):
        info_dialog = InfoDialog(title=title, message=message)
        info_dialog.exec()

    def new(self):
        self.viewer_container.new()
        self.update_layer_list()

    def open(self):
        self.viewer_container.open()
        self.update_layer_list()

    def save(self):
        if self.viewer_container.image is not None:
            self.viewer_container.save()

    def quit(self):
        if self.viewer_container.image is not None:
            self.viewer_container.quit()
        self.close()

    def clear(self):
        self.viewer_container.clear()
        self.update_layer_list()

    @staticmethod
    def about():
        about_dialog = AboutDialog()
        about_dialog.exec()

    @staticmethod
    def help():
        return 0

    def select_brush(self):
        self.viewer_container.tool = "brush"

    def select_line(self):
        self.viewer_container.tool = "line"

    def select_square(self):
        self.viewer_container.tool = "square"

    def select_circle(self):
        self.viewer_container.tool = "circle"

    def pen_size_slider_changed(self):
        self.brush_thickness_text.setValue(self.brush_thickness.value())
        self.viewer_container.set_thickness(self.brush_thickness.value())

    def pen_size_text_changed(self):
        self.brush_thickness.setValue(int(self.brush_thickness_text.value()))
        self.viewer_container.set_thickness(self.brush_thickness_text.value())

    def brush_line_style_changed(self):
        self.viewer_container.set_line_style(self.brush_line_type.currentIndex() + Qt.SolidLine)

    def brush_cap_type_changed(self):
        self.viewer_container.set_cap_style(self.brush_cap_type.currentIndex() * Qt.SquareCap)

    def brush_join_type_changed(self):
        self.viewer_container.set_join_style(self.brush_join_type.currentIndex() * Qt.BevelJoin)

    def pick_color(self):
        color_dialog = QColorDialog()
        color_dialog.exec()
        self.viewer_container.set_color(color_dialog.currentColor())

    def layer_changed(self):
        self.remove_layer_button.setDisabled(True if self.current_layer() == 0 else False)
        self.viewer_container.pos = self.current_layer()

    def layer_list_right_click(self, pos):
        item = self.layer_list.itemAt(pos)
        if item is not None:
            list_pos = self.layer_list.row(item)
            layer = self.viewer_container.get_layers()[list_pos]
            menu = QMenu(layer.name, self)

            hide_action = QAction(self.visible_icon if layer.visible else self.hidden_icon,
                                  "Hide" if layer.visible else "Show", menu)
            hide_action.triggered.connect(partial(self.layer_visibility_toggle, list_pos))
            menu.addAction(hide_action)

            rename_action = QAction("Rename", menu)
            rename_action.triggered.connect(partial(self.rename_layer, pos))
            menu.addAction(rename_action)

            menu.exec_(self.layer_list.mapToGlobal(pos))

    def layer_visibility_toggle(self, pos):
        self.viewer_container.get_layers()[pos].visible = not self.viewer_container.get_layers()[pos].visible
        self.update_layer_list()
        self.viewer_container.draw_image()

    def rename_layer(self, pos):
        try:
            self.layer_list.itemChanged.disconnect()
        except TypeError:
            pass
        self.layer_list.itemChanged.connect(
            partial(self.layer_renamed, self.layer_list.row(self.layer_list.itemAt(pos)))
        )
        self.layer_list.edit(self.layer_list.indexAt(pos))

    def layer_renamed(self, pos):
        self.viewer_container.get_layers()[pos].name = self.layer_list.item(pos).text()
        self.update_layer_list()
        self.layer_list.setCurrentRow(self.layer_list.row(self.layer_list.item(pos)))

    def add_layer(self):
        index = self.layer_list.currentRow()
        self.viewer_container.add_layer(index + 1)
        self.update_layer_list()
        self.layer_list.setCurrentRow(index + 1)

    def remove_layer(self):
        index = self.layer_list.currentRow()
        if index != 0:
            self.viewer_container.delete_layer(index)
            self.update_layer_list()
            self.layer_list.setCurrentRow(index - 1 if index == self.viewer_container.get_layer_nbr() else index)

    def setup_ui(self):
        # basic window
        self.setWindowTitle('Paint Application - Assignment 2 - Kevin Ferdinand - 2981444')
        self.setGeometry(560, 240, 800, 600)

        # layout setting
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self.make_menu()
        self.make_top_menu()
        self.make_right_menu()
        self.make_image_viewer()

        # add layout
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        widget = QWidget()
        widget.setLayout(self.grid)
        self.setCentralWidget(widget)
        self.show()

    def make_image_viewer(self):
        scroll_area = QScrollArea()
        scroll_area.setWidget(self.viewer_container)
        scroll_area.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.grid.addWidget(scroll_area, 2, 0, -1, 9)

    def make_right_menu(self):
        # right menu
        right_menu_box = QGroupBox()
        self.grid.addWidget(right_menu_box, 2, 9, -1, -1)
        right_menu_layout = QGridLayout()
        right_menu_box.setLayout(right_menu_layout)

        # groupbox title margin
        # right_menu_box.setStyleSheet("QGroupBox{ margin-top:-23px; }")

        # layers label
        label = QLabel("Layers:")
        right_menu_layout.addWidget(label, 0, 0)

        # layers list
        right_menu_layout.addWidget(self.layer_list, 1, 0, 9, -1)
        self.layer_list.currentRowChanged.connect(self.layer_changed)
        self.layer_list.setContextMenuPolicy(Qt.CustomContextMenu)
        self.layer_list.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.layer_list.customContextMenuRequested.connect(self.layer_list_right_click)

        # add button
        self.add_layer_button.setIcon(QIcon("img/plus.png"))
        self.add_layer_button.clicked.connect(self.add_layer)
        right_menu_layout.addWidget(self.add_layer_button, 10, 0)
        self.add_layer_button.setDisabled(True)

        # remove button
        self.remove_layer_button.setIcon(QIcon("img/delete.png"))
        self.remove_layer_button.clicked.connect(self.remove_layer)
        right_menu_layout.addWidget(self.remove_layer_button, 10, 1)
        self.remove_layer_button.setDisabled(True)

    def make_top_menu(self):
        # top menu
        top_menu_box = QGroupBox()
        self.grid.addWidget(top_menu_box, 0, 0, 1, -1)
        top_menu_layout = QGridLayout()
        top_menu_box.setLayout(top_menu_layout)

        # groupbox title padding
        # top_menu_box.setStyleSheet("QGroupBox{ margin-top:-23px; }")

        # drawing tools groupbox
        drawing_tools_groupbox = QGroupBox()
        drawing_tools_groupbox.setTitle("Drawing tools")
        drawing_tools_layout = QGridLayout()
        drawing_tools_groupbox.setLayout(drawing_tools_layout)
        top_menu_layout.addWidget(drawing_tools_groupbox, 0, 0, 2, 2)

        # brush button
        brush_button = QPushButton()
        brush_button.setIcon(QIcon("img/brush.png"))
        brush_button.clicked.connect(self.select_brush)
        drawing_tools_layout.addWidget(brush_button, 0, 0, 2, 2)

        # line tool
        line_button = QPushButton()
        line_button.setIcon(QIcon("img/line.png"))
        line_button.clicked.connect(self.select_line)
        drawing_tools_layout.addWidget(line_button, 0, 2, 2, 2)

        # square button
        square_button = QPushButton()
        square_button.setIcon(QIcon("img/square.png"))
        square_button.clicked.connect(self.select_square)
        drawing_tools_layout.addWidget(square_button, 0, 4, 2, 2)

        # circle button
        circle_button = QPushButton()
        circle_button.setIcon(QIcon("img/circle.png"))
        circle_button.clicked.connect(self.select_circle)
        drawing_tools_layout.addWidget(circle_button, 0, 6, 2, 2)

        # brush size
        pen_size_groupbox = QGroupBox()
        pen_size_groupbox.setTitle("Pen size")
        pen_size_layout = QGridLayout()
        pen_size_groupbox.setLayout(pen_size_layout)
        self.brush_thickness.setRange(1, 150)
        self.brush_thickness.valueChanged.connect(self.pen_size_slider_changed)
        self.brush_thickness_text.setValue(self.brush_thickness.value())
        self.brush_thickness_text.setRange(1, 10000)
        self.brush_thickness_text.valueChanged.connect(self.pen_size_text_changed)
        pen_size_layout.addWidget(self.brush_thickness, 0, 0, 1, 9)
        pen_size_layout.addWidget(self.brush_thickness_text, 0, 9, 1, 1)
        top_menu_layout.addWidget(pen_size_groupbox, 0, 6, 2, 2)

        # line style combo box
        line_style_groupbox = QGroupBox()
        line_style_groupbox.setTitle("Line style")
        line_style_layout = QGridLayout()
        line_style_groupbox.setLayout(line_style_layout)
        self.brush_line_type.setToolTip("Line style")
        self.brush_line_type.setEditable(False)
        self.brush_line_type.setIconSize(QSize(100, 14))
        self.brush_line_type.setMinimumWidth(100)
        self.brush_line_type.currentIndexChanged.connect(self.brush_line_style_changed)
        styles = range(Qt.SolidLine, Qt.CustomDashLine)
        for style in styles:
            image = QImage(100, 14, QImage.Format_ARGB32)
            image.fill(Qt.transparent)
            brush = QBrush(Qt.black)
            pen = QPen(brush, 2.5, style)

            painter = QPainter(image)
            painter.setPen(pen)
            painter.drawLine(2, 7, 98, 7)
            painter.end()
            self.brush_line_type.addItem(QIcon(QPixmap.fromImage(image)), "")
        line_style_layout.addWidget(self.brush_line_type)
        top_menu_layout.addWidget(line_style_groupbox, 0, 8, 2, 2)

        # cap style
        cap_style_groupbox = QGroupBox()
        cap_style_groupbox.setTitle("Cap style")
        cap_style_layout = QGridLayout()
        cap_style_groupbox.setLayout(cap_style_layout)
        self.brush_cap_type.addItem("Flat cap")
        self.brush_cap_type.addItem("Square cap")
        self.brush_cap_type.addItem("Round cap")
        self.brush_cap_type.currentIndexChanged.connect(self.brush_cap_type_changed)
        cap_style_layout.addWidget(self.brush_cap_type)
        top_menu_layout.addWidget(cap_style_groupbox, 0, 10, 2, 2)

        # cap style
        join_style_groupbox = QGroupBox()
        join_style_groupbox.setTitle("Join style")
        join_style_layout = QGridLayout()
        join_style_groupbox.setLayout(join_style_layout)
        self.brush_join_type.addItem("Miter join")
        self.brush_join_type.addItem("Bevel join")
        self.brush_join_type.addItem("Round join")
        self.brush_join_type.currentIndexChanged.connect(self.brush_join_type_changed)
        join_style_layout.addWidget(self.brush_join_type)
        top_menu_layout.addWidget(join_style_groupbox, 0, 12, 2, 2)

        # color picker
        color_groupbox = QGroupBox()
        color_groupbox.setTitle("Color")
        color_layout = QGridLayout()
        color_groupbox.setLayout(color_layout)
        color_button = QPushButton()
        color_button.setIcon(QIcon("img/color-palette.png"))
        color_button.clicked.connect(self.pick_color)
        color_layout.addWidget(color_button)
        top_menu_layout.addWidget(color_groupbox, 0, 14, 2, 2)

    def make_menu(self):
        # menus
        menu = self.menuBar()
        file_menu = menu.addMenu("File")
        help_menu = menu.addMenu("Help")

        # file_menu -> Open
        new_action = QAction(QIcon("img/new.png"), "New", self)
        new_action.setShortcut("Ctrl+N")
        file_menu.addAction(new_action)
        new_action.triggered.connect(self.new)

        # file_menu -> Open
        open_action = QAction(QIcon("img/open.png"), "Open", self)
        open_action.setShortcut("Ctrl+O")
        file_menu.addAction(open_action)
        open_action.triggered.connect(self.open)

        # file_menu -> Save
        save_action = QAction(QIcon("img/save.png"), "Save", self)
        save_action.setShortcut("Ctrl+S")
        file_menu.addAction(save_action)
        save_action.triggered.connect(self.save)

        # file_menu -> Clear
        clear_action = QAction(QIcon("img/delete.png"), "Clear", self)
        clear_action.setShortcut("Ctrl+X")
        file_menu.addAction(clear_action)
        clear_action.triggered.connect(self.clear)

        # file_menu -> Quit
        quit_action = QAction(QIcon("img/exit.png"), "Quit", self)
        quit_action.setShortcut("Ctrl+Q")
        file_menu.addAction(quit_action)
        quit_action.triggered.connect(self.quit)

        # help_menu -> About
        about_action = QAction(QIcon("img/about.png"), "About", self)
        about_action.setShortcut("Ctrl+A")
        help_menu.addAction(about_action)
        about_action.triggered.connect(Paint.about)

        # help_menu -> About
        help_action = QAction("Help", self)
        help_action.setShortcut("Ctrl+H")
        help_menu.addAction(help_action)
        help_action.triggered.connect(Paint.help)
