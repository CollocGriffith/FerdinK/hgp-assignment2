from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QLineEdit, QFormLayout, QLabel, QDialogButtonBox

from sources.InfoDialog import InfoDialog


class SizeDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        layout = QFormLayout(self)

        label_width = QLabel("Width :")
        self.le_width = QLineEdit(self)
        layout.addRow(label_width, self.le_width)

        label_height = QLabel("Height :")
        self.le_height = QLineEdit(self)
        layout.addRow(label_height, self.le_height)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.accepted.connect(self.accepted)
        button_box.rejected.connect(self.rejected)

        self.isAccepted = False

    def accepted(self):
        if not self.le_width.text().isdigit() or not self.le_height.text().isdigit():
            self.info_dialog("Error", "Width and Height must be filled and must be digits only.")
        else:
            self.isAccepted = True
            self.close()

    def rejected(self):
        self.close()

    @staticmethod
    def info_dialog(title, message):
        info_dialog = InfoDialog(title=title, message=message)
        info_dialog.exec()

    def is_accepted(self):
        return self.isAccepted

    def get_width(self):
        return int(self.le_width.text())

    def get_height(self):
        return int(self.le_height.text())
