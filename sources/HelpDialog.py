from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QFormLayout, QLabel, QDialogButtonBox, QWidget, QGridLayout, QComboBox


class HelpDialog(QDialog):
    def __init__(self, parent=None, title="", message=""):
        super().__init__(parent)

        self.form_layout = QFormLayout(self)
        self.selector = QComboBox()
        self.selector.addItem("Menu")
        self.selector.addItem("Tools")
        self.selector.addItem("Layers")
        self.form_layout.addRow(QLabel("Select category"), self.selector)
        self.selector.currentIndexChanged.connect(self.index_changed)
        self.widget = QWidget()
        self.form_layout.addRow(self.widget)
        self.layout = QGridLayout(self.widget)
        self.widget.setLayout(self.layout)
        button_box = QDialogButtonBox(QDialogButtonBox.Close, Qt.Horizontal, self)
        self.form_layout.addRow(button_box)
        button_box.rejected.connect(self.close)
        self.selector.setCurrentIndex(0)

    def index_changed(self):
        widget = QWidget()
        self.form_layout.replaceWidget(self.widget, widget)
        del self.widget
        self.widget = widget
        self.layout = QGridLayout(self.widget)
        self.widget.setLayout(self.layout)

        if self.selector.currentIndex() == 0:
            self.do_menu_help()
        elif self.selector.currentIndex() == 1:
            self.do_tools_help()
        elif self.selector.currentIndex() == 2:
            self.do_layers_help()

    def do_menu_help(self):
        label = QLabel("Menus:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 0, 0)
        label = QLabel("File:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 1, 1)
        label = QLabel("New:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 2, 2)
        # describe
        label = QLabel("Open:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 3, 2)
        # describe
        label = QLabel("Save:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 4, 2)
        # describe
        label = QLabel("Clear:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 5, 2)
        # describe
        label = QLabel("Quit:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 6, 2)
        # describe

    def do_tools_help(self):
        label = QLabel("Menus:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 0, 0)
        label = QLabel("File:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 1, 1)
        label = QLabel("New:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 2, 2)
        # describe
        label = QLabel("Open:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 3, 2)
        # describe
        label = QLabel("Save:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 4, 2)
        # describe
        label = QLabel("Clear:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 5, 2)
        # describe
        label = QLabel("Yolo:", self.widget)
        label.setStyleSheet("font-weight: bold;")
        self.layout.addWidget(label, 6, 2)

    def do_layers_help(self):
        print('lol')