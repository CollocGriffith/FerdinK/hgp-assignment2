from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QFormLayout, QLabel, QDialogButtonBox


class InfoDialog(QDialog):
    def __init__(self, parent=None, title="", message=""):
        super().__init__(parent)

        layout = QFormLayout(self)

        self.setWindowTitle(title)
        layout.addRow(QLabel(message))

        button_box = QDialogButtonBox(QDialogButtonBox.Ok, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.accepted.connect(self.btn_click)

    def btn_click(self):
        self.close()
