import pickle
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QImageReader, QPainter


class Layer():
    def __init__(self, image, name):
        self.image = image
        self.visible = True
        self.name = name


class Image:
    def __init__(self, path=True, file_path=None, width=0, height=0):
        self.layers = []
        if path:
            reader = QImageReader(file_path)
            reader.setAutoTransform(True)
            if reader.canRead():
                self.valid = True
                image = reader.read()
                image.convertToFormat(QImage.Format_ARGB32)
                self.width = image.width()
                self.height = image.height()
                self.img_format = QImage.Format_ARGB32
                self.layers.append(Layer(image, "Base layer"))
            else:
                self.valid = False
        else:
            self.layers.append(Layer(QImage(width, height, QImage.Format_ARGB32), "Base layer"))
            self.layers[0].image.fill(Qt.white)
            self.width = width
            self.height = height
            self.img_format = QImage.Format_ARGB32
            self.valid = True

    def is_valid(self):
        return self.valid

    def delete_layer(self, position):
        del self.layers[position]

    def add_layer(self, position):
        layer = QImage(self.width, self.height, self.img_format)
        layer.fill(Qt.transparent)
        self.layers.insert(position, Layer(layer, "Layer " + str(self.get_layer_nbr())))

    def delete_all_layers(self):
        while len(self.layers) > 1:
            self.layers.pop(len(self.layers) - 1)
        self.layers[0].image.fill(Qt.white)

    def get_image(self, drawing_layer=None, drawing_layer_position=-1):
        image = QImage(self.width, self.height, self.img_format)
        image.fill(Qt.transparent)
        painter = QPainter(image)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        index = 0
        for layer in self.layers:
            if layer.visible:
                painter.drawImage(0, 0, layer.image)
                if drawing_layer and drawing_layer_position == index:
                    painter.drawImage(0, 0, drawing_layer)
            index += 1
        painter.end()
        return image

    def draw(self, image, position):
        painter = QPainter(self.layers[position].image)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawImage(0, 0, image)
        painter.end()

    def get_layer_nbr(self):
        return len(self.layers)

    def get_layers(self):
        return self.layers
