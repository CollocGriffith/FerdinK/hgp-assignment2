from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QFormLayout, QDialogButtonBox, QLabel


class AboutDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        layout = QFormLayout(self)
        layout.addRow(QLabel("Version: 1.0.0"))
        layout.addRow(QLabel("Created by Kevin FERDINAND - 2981444"))
        button_box = QDialogButtonBox(QDialogButtonBox.Close, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.rejected.connect(self.close)
