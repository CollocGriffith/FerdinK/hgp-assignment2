from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QPixmap, QMouseEvent, QPainter, QPen, QImage
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QFileDialog

from sources.Image import Image
from sources.InfoDialog import InfoDialog
from sources.SizeDialog import SizeDialog
from sources.YesNoDialog import YesNoDialog


class ImageViewer(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.drawing = False
        self.last_pos = None
        self.tool = "brush"
        self.pen_size = 1
        self.line_style = Qt.SolidLine
        self.cap_style = Qt.SquareCap
        self.join_style = Qt.BevelJoin
        self.pen_color = Qt.black
        self.viewer = QLabel()
        self.layout = QGridLayout()
        self.layout.addWidget(self.viewer)
        self.setLayout(self.layout)
        self.drawing_layer = None
        self.image = None
        self.pos = 0
        self.saved = True
        self.open_format = "Images (*.bmp *.gif *.jpg *.jpeg *.png)"
        self.save_format = "Windows bitmap (.bmp);;Graphic Interchange Format (.gif);;Joint Photographic Experts " + \
                           "Group(.jpg);;Joint Photographic Experts Group (.jpeg);;Portable Network Graphics (.png)"

    def draw_image(self):
        if self.drawing_layer is None:
            self.drawing_layer = QImage(self.image.width, self.image.height, QImage.Format_ARGB32)
            self.drawing_layer.fill(Qt.transparent)
        to_display = self.image.get_image(self.drawing_layer, self.pos)
        self.viewer.setPixmap(QPixmap.fromImage(to_display))
        self.viewer.resize(to_display.size())
        self.resize(to_display.size())
            
    def mousePressEvent(self, event: QMouseEvent):
        if event.button() == Qt.LeftButton and self.viewer.underMouse():
            self.drawing = True
            self.last_pos = self.viewer.mapFromGlobal(self.mapToGlobal(event.pos()))

    def mouseReleaseEvent(self, event: QMouseEvent):
        if event.button() == Qt.LeftButton:
            self.drawing = False
            self.last_pos = None
            self.image.draw(self.drawing_layer, self.pos)
            self.drawing_layer.fill(Qt.transparent)

    def mouseMoveEvent(self, event: QMouseEvent):
        if (event.buttons() & Qt.LeftButton) & self.drawing:
            pos = self.viewer.mapFromGlobal(self.mapToGlobal(event.pos()))
            if self.tool == "brush":
                self.brush_tool(pos)
            elif self.tool == "line":
                self.line_tool(pos)
            elif self.tool == "square":
                self.square_tool(pos)
            elif self.tool == "circle":
                self.circle_tool(pos)
            self.draw_image()

    def brush_tool(self, position):
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawLine(self.last_pos, position)
        self.last_pos = position
        painter.end()
        self.saved = False

    def line_tool(self, position):
        self.drawing_layer.fill(Qt.transparent)
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawLine(self.last_pos, position)
        painter.end()
        self.saved = False

    def square_tool(self, position):
        self.drawing_layer.fill(Qt.transparent)
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawRect(QRect(self.last_pos, position))
        painter.end()
        self.saved = False

    def circle_tool(self, position):
        self.drawing_layer.fill(Qt.transparent)
        painter = QPainter(self.drawing_layer)
        painter.setPen(QPen(self.pen_color, self.pen_size, self.line_style, self.cap_style, self.join_style))
        painter.drawEllipse(QRect(self.last_pos, position))
        painter.end()
        self.saved = False

    @staticmethod
    def info_dialog(title, message):
        info_dialog = InfoDialog(title=title, message=message)
        info_dialog.exec()

    def new(self):
        size_dialog = SizeDialog()
        size_dialog.exec()
        if size_dialog.is_accepted():
            width = size_dialog.get_width()
            height = size_dialog.get_height()
            self.image = Image(path=False, width=width, height=height)
            if self.image.is_valid():
                self.draw_image()
                self.drawing_layer = QImage(width, height, QImage.Format_ARGB32)
                self.drawing_layer.fill(Qt.transparent)
                self.saved = False
            else:
                ImageViewer.info_dialog("Error", "Something went wrong when loading image, try again.")

    def open(self):
        if not self.saved:
            self.ask_save()
        file, _ = QFileDialog.getOpenFileName(self, "Select image", "/home", self.open_format)
        if file:
            self.image = Image(file_path=file)
            if self.image.is_valid():
                self.draw_image()
                self.drawing_layer = QImage(self.image.width, self.image.height, QImage.Format_ARGB32)
                self.drawing_layer.fill(Qt.transparent)
            else:
                ImageViewer.info_dialog("Error", "Something went wrong when loading image, try again.")

    def save(self):
        file, ext = QFileDialog.getSaveFileName(self, "Select where to save image", "/home", self.save_format)
        if file and ext:
            self.image.get_image().save(file + ext.split('(')[1].split(')')[0])
            self.saved = True

    def ask_save(self):
        ask_dialog = YesNoDialog(title="Work not saved", message="You have unsaved changes, save now ?")
        ask_dialog.exec()
        if ask_dialog.is_accepted():
            self.save()

    def clear(self):
        if self.image is not None:
            self.image.delete_all_layers()
            self.draw_image()
            
    def quit(self):
        if not self.saved:
            self.ask_save()

    def add_layer(self, position):
        self.image.add_layer(position)
        self.saved = False

    def delete_layer(self, position):
        self.image.delete_layer(position)
        self.draw_image()

    def get_layers(self):
        return self.image.get_layers()

    def get_layer_nbr(self):
        return self.image.get_layer_nbr()

    def set_thickness(self, thickness):
        self.pen_size = thickness

    def set_line_style(self, style):
        self.line_style = style

    def set_cap_style(self, style):
        self.cap_style = style

    def set_join_style(self, style):
        self.join_style = style

    def set_color(self, color):
        self.pen_color = color
