from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QFormLayout, QLabel, QDialogButtonBox


class YesNoDialog(QDialog):
    def __init__(self, parent=None, title="", message=""):
        super().__init__(parent)

        layout = QFormLayout(self)

        self.setWindowTitle(title)
        layout.addRow(QLabel(message))

        button_box = QDialogButtonBox(QDialogButtonBox.Yes | QDialogButtonBox.No, Qt.Horizontal, self)
        layout.addRow(button_box)

        button_box.accepted.connect(self.accepted)
        button_box.rejected.connect(self.rejected)
        self.isAccepted = False

    def is_accepted(self):
        return self.isAccepted

    def accepted(self):
        self.isAccepted = True
        self.close()

    def rejected(self):
        self.close()
