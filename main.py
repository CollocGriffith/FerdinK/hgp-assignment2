import sys
from PyQt5.QtWidgets import QApplication, QStyleFactory

from sources.Paint import Paint

__author__ = 'Kevin Ferdinand'
__contact__ = 'kevin.ferdinand@epitech.eu'
__copyright__ = 'Kevin Ferdinand 2018'
__date__ = '01/10/2018'
__version__ = '0.0.1'

if __name__ == '__main__':
    app = QApplication(sys.argv)
    paint = Paint()
    exit(app.exec())
